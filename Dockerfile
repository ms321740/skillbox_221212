FROM python:3-alpine

WORKDIR /code

COPY ./code/requirements.txt .
RUN pip install -r requirements.txt
RUN mkdir db
COPY . /code

VOLUME /code/db

EXPOSE  8000

CMD sh init.sh && python manage.py runserver 0.0.0.0:8000
